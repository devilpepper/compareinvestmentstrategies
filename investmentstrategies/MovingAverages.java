package investmentstrategies;

import java.util.Map;
import java.util.HashMap;
import data.MarketSnapshot;
import data.MutableInt;

public class MovingAverages extends InvestmentStrategy {
	private MarketSnapshot[] tenDayQuotes;
	private MarketSnapshot[] thirtyDayQuotes;
	private Map<String, Double> tenDayAvg;
	private Map<String, Double> thirtyDayAvg;
	private int tenDayIndex;
	private int thirtyDayIndex;

	// The array of MarketSnapshot is the previous 30 days stock quotes.
	public MovingAverages(double investmentCapital, MarketSnapshot[] thirtyDayQuotes)
	{
		super(investmentCapital);
		this.tenDayQuotes = new MarketSnapshot[10];
		this.thirtyDayQuotes = new MarketSnapshot[30];
		for (int i = 0; i < 30; ++i) {
			this.tenDayQuotes[i%10] = thirtyDayQuotes[i%10];
			this.thirtyDayQuotes[i] = thirtyDayQuotes[i];
		}
		tenDayIndex = 0;
		thirtyDayIndex = 0;
		tenDayAvg = new HashMap<String, Double>();
		thirtyDayAvg = new HashMap<String, Double>();

		for (String key : tenDayQuotes[0].getSnapshot().keySet()) {
			tenDayAvg.put(key, calculate10DayAverage(key));
			thirtyDayAvg.put(key, calculate30DayAverage(key));
			portfolio.put(key, new MutableInt());
		}
	}

	public void updatePortfolio(MarketSnapshot quotes)
	{
		tenDayQuotes[tenDayIndex] = quotes;
		thirtyDayQuotes[thirtyDayIndex] = quotes;
		tenDayIndex = (tenDayIndex + 1) % 10;
		thirtyDayIndex = (thirtyDayIndex + 1) % 30;

		for (String stock : portfolio.keySet()) {
			updateStock(stock, quotes.getStockQuote(stock).getPrice());
		}

		calculatePortfolioValue(quotes);
	}

	private void updateStock(String stock, double price)
	{
		double prev10DayAvg = tenDayAvg.get(stock);
		double prev30DayAvg = thirtyDayAvg.get(stock);
		double tenDayAvg = calculate10DayAverage(stock);
		double thirtyDayAvg = calculate30DayAverage(stock);

		if (tenDayAvg > thirtyDayAvg && prev10DayAvg < prev30DayAvg) {
			buyStock(stock, price, 100);
		}
		else if (tenDayAvg < thirtyDayAvg && prev10DayAvg > prev30DayAvg) {
			sellStock(stock, price, 100);
		}

	}

	private void buyStock(String stock, double price, int amt) 
	{
		while (price < investmentCapital && amt > 0) {
			investmentCapital -= price;
			amt -= 1;
			portfolio.get(stock).add(1);
		}
	}

	private void sellStock(String stock, double price, int amt) 
	{
		while (amt > 0 && portfolio.get(stock).get() > 0) {
			investmentCapital += price;
			amt -= 1;
			portfolio.get(stock).sub(1);
		}
	}

	private double calculate10DayAverage(String stock)
	{
		double result = 0.0;
		for (int i = 0; i < 10; ++i) {
			result += tenDayQuotes[i].getStockQuote(stock).getPrice();
		}
		return result / 10;
	}

	private double calculate30DayAverage(String stock)
	{
		double result = 0.0;
		for (int i = 0; i < 30; ++i) {
			result += thirtyDayQuotes[i].getStockQuote(stock).getPrice();
		}
		return result / 30;
	}
}
