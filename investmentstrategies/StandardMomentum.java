package investmentstrategies;


public class StandardMomentum extends Momentum {
	public StandardMomentum(double investmentCapital)
	{
		super(investmentCapital);
		this.BUY = 1;
		this.SELL = -1;
	}
}

