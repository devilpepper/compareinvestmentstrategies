package investmentstrategies;

public class InverseMomentum extends Momentum {
	public InverseMomentum(double investmentCapital)
	{
		super(investmentCapital);
		this.BUY = -1;
		this.SELL = 1;
	}
}
