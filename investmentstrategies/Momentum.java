package investmentstrategies;

//even though this extends InvestmentStrategy, I need to import these...
import data.StockQuote;
import data.MarketSnapshot;
import data.MutableInt;

import java.util.ArrayList;
import java.util.Map;

public abstract class Momentum extends InvestmentStrategy {
	private ArrayList<Map<String, StockQuote>> Q;
	private final int HIST_LENGTH = 10;
	protected int BUY, SELL;

	public Momentum(double investmentCapital)
	{
		super(investmentCapital);
		Q = new ArrayList<Map<String, StockQuote>>();
	}

	public void updatePortfolio(MarketSnapshot quotes)
	{
		add2Q(quotes.getSnapshot());
		sell();
		buy();
		calculatePortfolioValue(quotes);
	}

	private void add2Q(Map<String, StockQuote> quotes)
	{
		if(Q.size() >= HIST_LENGTH) Q.remove(0);
		Q.add(quotes);
	}

	int idTrend(String symbol)
	{
		//some non trivial calculation
		int snapshots = Q.size();
		if(snapshots > 4) //4 is too small to see trend
		{
			double critical = Q.get(0).get(symbol).getPrice();
			double current, last = Q.get(1).get(symbol).getPrice();
			double sum = 0;
			boolean rising = (last > critical);
			for(int i = 2; i < snapshots; i++)
			{
				current = Q.get(i).get(symbol).getPrice();
				if(rising)
				{
					if(current < last)
					{
						rising = false;
						sum += current - critical;
						critical = current;
					}
				}
				else
				{
					if(current > last)
					{
						rising = false;
						sum += current - critical;
						critical = current;
					}
				}
				last = current;
			}

			if(sum > 0)
				return 1;
			else if(sum <0) return -1;
		}

		return 0;

		//here's a trivial one
		/*
		if(Q.size() < HIST_LENGTH)
			return 0;
		else if (Q.get(0).get(symbol).getPrice() * 1.25 < Q.get(HIST_LENGTH-1).get(symbol).getPrice())
			return 1;
		else if (Q.get(HIST_LENGTH-1).get(symbol).getPrice() * 1.25 < Q.get(0).get(symbol).getPrice())
			return -1;
		else 
			return 0;
		*/
	}

	private void sell()
	{
		String symbol;
		ArrayList<String> deleteit = new ArrayList<String>();
		double price;
		for (Map.Entry<String, MutableInt> holding : this.portfolio.entrySet())
		{
			symbol = holding.getKey();
			if(idTrend(symbol) == SELL)
			{
				price = Q.get(Q.size()-1).get(symbol).getPrice() * 200;
				//System.out.printf("Selling 200 shares of %s for $%f\n", symbol, price);
				this.investmentCapital += price;
				this.portfolio.get(symbol).sub(200);
				//System.out.printf("New capital: $%f\nShares left:%d\n-------------\n",
						//this.investmentCapital, this.portfolio.get(symbol).get());

				if(this.portfolio.get(symbol).get() == 0)
					deleteit.add(symbol);
				if(this.portfolio.get(symbol).get() < 0) while(true);
			}
		}
		for (String sellStock : deleteit)
		{
			//System.out.printf("Removing %s\n", sellStock);
			this.portfolio.remove(sellStock);
		}
	}

	private void buy()
	{
		String symbol;
		double price;
		for (Map.Entry<String, StockQuote> stock : this.Q.get(0).entrySet())
		{
			symbol = stock.getKey();
			if(idTrend(symbol) == BUY)
			{
				price = Q.get(Q.size()-1).get(symbol).getPrice() * 200;
				//System.out.printf("Buying 200 shares of %s for $%f\n", symbol, price);
				if(price <= this.investmentCapital){
					this.investmentCapital -= price;
					if(this.portfolio.get(symbol) == null) this.portfolio.put(symbol, new MutableInt());
					this.portfolio.get(symbol).add(200);
					//System.out.printf("New capital: $%f\nShares left:%d\n-------------",
						//this.investmentCapital, this.portfolio.get(symbol).get());

				}
				//else System.out.printf("Can't afford %s\n", symbol);
			}
		}
	}
}
