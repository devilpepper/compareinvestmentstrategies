package investmentstrategies;

import data.MarketSnapshot;
import data.StockQuote;
import java.util.Map;
import data.MutableInt;

public class BuyIndex extends InvestmentStrategy {
	private boolean day1;
	private final int NUM_TO_BUY = 1;
	//if investmentCapital = 25799.34, can buy 1 of each stock in index 1 on day 1
	public BuyIndex(double investmentCapital)
	{
		super(investmentCapital);
		this.day1 = true;
	}

	public void updatePortfolio(MarketSnapshot quotes)
	{
		if(day1)
		{
			day1 = false;
			Map<String, StockQuote> snapshot1 = quotes.getSnapshot();
			
			String symbol;
			double price;
			for (Map.Entry<String, StockQuote> stock : snapshot1.entrySet())
			{
				symbol = stock.getKey();
				price = stock.getValue().getPrice() * NUM_TO_BUY;
					if(price <= this.investmentCapital){
						this.investmentCapital -= price;
						if(this.portfolio.get(symbol) == null) this.portfolio.put(symbol, new MutableInt());
						this.portfolio.get(symbol).add(NUM_TO_BUY);
				}
			}
		}
		calculatePortfolioValue(quotes);
	}
}
