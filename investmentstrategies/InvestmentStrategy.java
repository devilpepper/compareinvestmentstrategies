/* Public Methods
 * --------------
 * Constructor(double investmentCapital)
 * double getPortfolioValue()
 * 
 * Abstract Method (To be defined)
 * -------------------------------
 * void updatePortfolio(MarketSnapshot quotes)
 * -Here make investment decisions based on MarketSnapshot and rules
 * -Purchases subtract from investmentCapital
 *  and add to portfolio map
 * -Selling subtracts from portfolio map
 *  and add to investmentCapital
 * -then call calculatePortfolioValue(MarketSnapshot quotes)
 */

package investmentstrategies;

import java.util.Map;
import java.util.HashMap;
import data.StockQuote;
import data.MarketSnapshot;
import data.MutableInt;

public abstract class InvestmentStrategy {
    protected Map<String, MutableInt> portfolio; //Maps don't like primitives
    // The initial value of all stocks in our portfolio.
    protected double initialValue;
    protected double portfolioValue;
    protected double investmentCapital;

	//for compiling other strategies before doing work
	public InvestmentStrategy()
	{
		this.initialValue = 0;
        this.portfolioValue = 0;
        this.investmentCapital = 0;
		this.portfolio = new HashMap<String, MutableInt>();
	}

    public InvestmentStrategy(double investmentCapital) {
        this.initialValue = investmentCapital;
        this.portfolioValue = investmentCapital;
        this.investmentCapital = investmentCapital;
		this.portfolio = new HashMap<String, MutableInt>();
    }

    /* Returns the portfolio value based on currentQuote */
    public double getPortfolioValue() { return portfolioValue; }

    protected void calculatePortfolioValue(MarketSnapshot quotes)
	{
		Map<String, StockQuote> updates = quotes.getSnapshot();
		this.portfolioValue = this.investmentCapital;
		for (Map.Entry<String, MutableInt> holding : this.portfolio.entrySet())
			this.portfolioValue += updates.get(holding.getKey()).getPrice() * holding.getValue().get();
	}
	
    /* This should Buy/Sell stocks in portfolio for a single time step
    ** (MarketSnapshot) based on the investment strategy rule.
    ** This should update the portfolio value by calling
    ** calculatePortfolioValue at the end. */
    public abstract void updatePortfolio(MarketSnapshot quotes);
	//{
        /* Do this last in child implementations:
        ** calculatePortfolioValue(quotes); */
	//}
}
