import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import dbqueries.DatabaseQueries;
import data.MarketSnapshot;
import investmentstrategies.InvestmentStrategy;
import investmentstrategies.MovingAverages;
import investmentstrategies.BuyIndex;
import investmentstrategies.StandardMomentum;
import investmentstrategies.InverseMomentum;
import java.awt.Desktop;
import java.io.File;

public final class Simulator {
    /* 1. Create an object for each investment strategy
    ** 2. Iterate through stock prices by creating a MarketSnapshot for some
    **    time step.
    ** 3. Update each investment strategy with the MarketSnapshot.
    ** 4. At the end of the simulation, output the initialValue and current portfolioValue
    **    for the investment strategy to compare. */

    private static Date start = new Date();
    private static Date end = new Date();
    private static double initialCapital;
    private static DateFormat formatter;

    private Simulator() {
        try {
            formatter = new SimpleDateFormat("MM/dd/yyyy");
            BufferedReader br = new BufferedReader(new FileReader("settings.txt"));;
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.contains("START_DATE="))
                    start = (Date)formatter.parse(line.substring(11));
                if (line.contains("END_DATE="))
                    end = (Date)formatter.parse(line.substring(9));
                if (line.contains("INITIAL_CAPITAL="))
                    initialCapital = Double.parseDouble(line.substring(16));
            }
        }
        catch (FileNotFoundException e) {
           System.err.println("Unable to find the file: fileName");
        }
        catch (IOException e) {
           System.err.println("Unable to read the file: fileName");
        }
        catch (ParseException e) {
            System.err.println("Bad date format");
        }
    }

    public static void main(String args[]) {
        // Set up Moving Averages
        // get previous 30 quotes
        new Simulator();
        DatabaseQueries.setupDB();
        Calendar cal = Calendar.getInstance();
        cal.setTime(start);
        MarketSnapshot[] thirtyDayQuotes = new MarketSnapshot[30];

        //System.out.println("Start Date: " + formatter.format(cal.getTime()));
        //System.out.println("End State: " + formatter.format(end));
        //System.out.println("Initial Capital: " + initialCapital);
        //System.out.println("Starting simulations...");

        int i = 29;
        int iterations = 0;
        //System.out.println("Loading previous 30 days trading data for moving averages...");
        while (i >= 0) {
            ++iterations;
            cal.add(Calendar.DATE, -1);
            // Need to check for weekends and other days without quotes
            MarketSnapshot ms = DatabaseQueries.getMarketSnapshot(1, cal.getTime());
            if (ms.getSnapshot().size() == 0) {
                continue;
            }
            else {
                thirtyDayQuotes[i] = ms;
                --i;
            }

            // to prevent infinite loop
            if (iterations > 100) {
                System.out.println("Please allow a 30 day lead time (excluding weekends) in the database to calculate moving averages");
                return;
            }
        }
        cal.setTime(start);

		InvestmentStrategy[] strats = new InvestmentStrategy[4];
		strats[0] = new MovingAverages(initialCapital, thirtyDayQuotes);
		strats[1] = new StandardMomentum(initialCapital);
		strats[2] = new InverseMomentum(initialCapital);
		strats[3] = new BuyIndex(initialCapital);
        //MovingAverages mvAvg = new MovingAverages(initialCapital, thirtyDayQuotes);
        ArrayList<String> dates = new ArrayList<String>();
        ArrayList<ArrayList<Double>> portfolioValues = new ArrayList<ArrayList<Double>>();

        dates.add(formatter.format(start));
        for (int j = 0; j < strats.length; ++j) {
            portfolioValues.add(new ArrayList<Double>());
            portfolioValues.get(j).add(strats[j].getPortfolioValue());
        }


        while (cal.getTime().before(end)) {
            Date prev = cal.getTime();
            cal.add(Calendar.DATE, 1);
            MarketSnapshot ms = DatabaseQueries.getMarketSnapshot(1, cal.getTime());
            // Need this since weekends and holidays will not have data.
            if (ms.getSnapshot().size() != 0) {
				for(InvestmentStrategy strat : strats)
				{
                	strat.updatePortfolio(ms);
					//System.out.printf("%s\t%f\n", strat, strat.getPortfolioValue());
				}
            }

            // Record the portfolio value at the start of each new month
            //if (prev.getMonth() != cal.getTime().getMonth()) {
                dates.add(formatter.format(cal.getTime()));
                for(int j = 0; j < strats.length; ++j) {
                    portfolioValues.get(j).add(strats[j].getPortfolioValue());
                }
            //}
        }

        System.out.println("Date,Moving Averages,Standard Momentum,Inverse Momentum,Buy Index");
        for (int j = 0; j < dates.size(); ++j) {
            System.out.print(dates.get(j));
            for (int k = 0; k < strats.length; ++k) {
                System.out.print("," + portfolioValues.get(k).get(j));
            }
            System.out.println();
         }

		/*
		//Now try to display the line graph
		try
		{
			File htmlFile = new File("linegraph/index.html");
			Desktop.getDesktop().browse(htmlFile.toURI());
		}
		catch(IOException e)
		{
			System.out.println("Couldn't render the line graphs :[");
		}
		*/
    }
}
