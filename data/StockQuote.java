package data;

public class StockQuote {
    private String stockSymbol;
    private double price, highPrice, lowPrice, startPrice, endPrice;

	public StockQuote(String stockSymbol, double price)
	{
		this.stockSymbol = stockSymbol;
		this.price = price;
        //this.price = (highPrice+lowPrice+startPrice+endPrice)/4; //some arbituary average. Not that great. No statistics involved...
		this.highPrice = price;
		this.lowPrice = price;
		this.startPrice = price;
		this.endPrice = price;
	}

    public StockQuote(String stockSymbol, double highPrice, double lowPrice, double startPrice, double endPrice) {
        this.stockSymbol = stockSymbol;
		this.price = endPrice;
        //this.price = (highPrice+lowPrice+startPrice+endPrice)/4; //some arbituary average. Not that great. No statistics involved...
		this.highPrice = highPrice;
		this.lowPrice = lowPrice;
		this.startPrice = startPrice;
		this.endPrice = endPrice;
    }

    public String getStockSymbol() { return stockSymbol; }
    public double getPrice()       { return price; }
    public double gethighPrice()   { return highPrice; }
    public double getlowPrice()    { return lowPrice; }
    public double getstartPrice()  { return startPrice; }
    public double getendPrice()    { return endPrice; }

	public String toString() { return "{" + this.stockSymbol + ", " + this.price + "}";}
}
