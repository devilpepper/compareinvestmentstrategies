package data;

public class MutableInt {
  int value;
  public MutableInt()      { value = 0;    }
  public void add  (int v) { value += v;   }
  public void sub  (int v) { value -= v;   }
  public int  get  ()      { return value; }
}
