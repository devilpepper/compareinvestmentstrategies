package data;

import java.util.Map;
import java.util.HashMap; //temporary to make it compile
//import comparestrategies.StockQuote;

// Contain stock quotes of relevant stocks at a single time step.
public class MarketSnapshot
{
    // Stock symbol -> StockQuote
    private Map<String, StockQuote> quotes;
	public MarketSnapshot() // temporary to make it compile
	{
		this.quotes = new HashMap<String, StockQuote>();
	}
    public MarketSnapshot(Map<String, StockQuote> quotes)
	{
        this.quotes = quotes;
    }

    public Map<String, StockQuote> getSnapshot() { return quotes; }

    public StockQuote getStockQuote(String stock) { return quotes.get(stock); }

	public String toString() { return "Symbol\tStockQuote\n" + this.quotes.toString(); }
}
