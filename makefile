JFLAGS = -g
JC = javac

CLASSES = \
		data/MarketSnapshot.java \
		data/MutableInt.java \
		data/StockQuote.java \
		data/Records.java \
		dbqueries/DatabaseQueries.java \
		investmentstrategies/BuyIndex.java \
		investmentstrategies/InverseMomentum.java \
		investmentstrategies/InvestmentStrategy.java \
		investmentstrategies/Momentum.java \
		investmentstrategies/MovingAverages.java \
		investmentstrategies/StandardMomentum.java \
		test.java \
		Simulator.java

COMPILED = \
		data/MarketSnapshot.class \
		data/MutableInt.class \
		data/StockQuote.class \
		data/Records.class \
		dbqueries/DatabaseQueries.class \
		investmentstrategies/BuyIndex.class \
		investmentstrategies/InverseMomentum.class \
		investmentstrategies/InvestmentStrategy.class \
		investmentstrategies/Momentum.class \
		investmentstrategies/MovingAverages.class \
		investmentstrategies/StandardMomentum.class \
		test.class \
		Simulator.class

default:
	$(JC) $(JFLAGS) $(CLASSES)

clean:
		$(RM) *.class

.PHONY : test
test:
	java -cp .:./mysql-connector-java-5.1.39-bin.jar test

.PHONY : simulate
simulate:
	java -cp .:./mysql-connector-java-5.1.39-bin.jar Simulator

.PHONY : csv
csv:
	java -cp .:./mysql-connector-java-5.1.39-bin.jar Simulator > data.csv

.PHONY : jar
jar:
	jar cfm InvestmentSimulator.jar ./Manifest $(COMPILED)
