function graphData(data) {
    var dates = [];
    var buyIndex = [];
    var inverseMomentum = [];
    var standardMomentum = [];
    var movingAverages = [];

    for(var i = 0; i < data.length; i++) {
        dates.push(data[i]['Date']);
        buyIndex.push(data[i]['Buy Index']);
        inverseMomentum.push(data[i]['Inverse Momentum']);
        standardMomentum.push(data[i]['Standard Momentum']);
        movingAverages.push(data[i]['Moving Averages'])
    }

    var buyIndexLine = {
        label: "Buy Index",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(75,192,192,1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: buyIndex,
    }

    var inverseMomentumLine = {
        label: "Inverse Momentum",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(255,98,98,0.4)",
        borderColor: "rgba(255,98,98,1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(255,98,98,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: inverseMomentum,
    }

    var standardMomentumLine = {
        label: "Standard Momentum",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(55,173,252,0.4)",
        borderColor: "rgba(55,173,252,1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(55,173,252,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: standardMomentum,
    }

    var movingAveragesLine = {
        label: "Moving Averages",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(173,82,242,0.4)",
        borderColor: "rgba(173,82,242,1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(173,82,242,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: movingAverages,
    }


    var data = {
        labels: dates,
        datasets: [ buyIndexLine,
                    inverseMomentumLine,
                    standardMomentumLine,
                    movingAveragesLine
                  ]
    };

    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data:data,
        options: {
            maintainAspectRatio: false,
            responsive: true
        }
    })
}
function handleFileSelect(evt) {
    var file = evt.target.files[0];

    Papa.parse(file, {
        header: true,
        skipEmptyLines: true,
        complete: function(results) {
            graphData(results.data);
        }
    });
}

$(document).ready(function(){
    $("#csv-file").change(handleFileSelect);
	/*
	$.ajax({
        type: "GET",
        url: "data.csv",
        dataType: "text",
        success: function(data) {handleFileSelect(data);}
     });
	 */
});
