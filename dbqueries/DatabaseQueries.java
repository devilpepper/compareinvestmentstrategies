/* Public Variables
 * ----------------
 * static DateFormat df
 *
 * Public Methods
 * --------------
 * static void setupDB()
 * static MarketSnapshot getMarketSnapshot(int marketIndexId, Date dateTime)
 * static Date getStartDate()
 * static Date getEndDate()
 * static String[] getTradingSymbols(int marketIndexId)
 */

package dbqueries;

import java.sql.*;
import data.MarketSnapshot;
import data.StockQuote;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.math.BigDecimal;

public final class DatabaseQueries 
{
	// JDBC driver name and database URL
   private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   private static String DB_URL = "jdbc:mysql://";
   private static String USER = "";
   private static String PASS = "";
   private static String DB = "";
   private static String DB_QUERY = "";
   private static String SYM_COL = "";
   private static String PRICE_COL = "";
   public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

   private DatabaseQueries(){}

   //Reads a file called cred.txt to get your database credentials.
   public static void setupDB()
   {
	   try {
		   BufferedReader br = new BufferedReader(new FileReader("cred.txt"));
		   USER = br.readLine();
		   PASS = br.readLine();
	   }
	   catch (FileNotFoundException e) {
		   System.err.println("Unable to find the file: fileName");
	   }
	   catch (IOException e) {
		   System.err.println("Unable to read the file: fileName");
	   }

	   try {
            BufferedReader br = new BufferedReader(new FileReader("settings.txt"));;
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.contains("DATABASE="))
                    DB = line.substring(9); //stockmarket
                if (line.contains("DATABASE_URL="))
                    DB_URL += line.substring(13); //134.74.126.107
				if(line.contains("SYMBOL_COL="))
					SYM_COL = line.substring(11);
				if(line.contains("PRICE_COL="))
					PRICE_COL = line.substring(10);
				if (line.contains("QUERY="))
				{
					DB_QUERY = line.substring(6);
					if(DB_QUERY.equals("default"))
						DB_QUERY = "select TRADE_DATE, TRADING_SYMBOL, OPEN_PRICE, CLOSE_PRICE, LOW_PRICE, HIGH_PRICE from STOCK_HISTORY, (select INSTRUMENT_ID from INDEX_CMPSTN where MARKET_INDEX_ID = 1) as banana where STOCK_HISTORY.INSTRUMENT_ID = banana.INSTRUMENT_ID and TRADE_DATE =";
				}

            }
        }
        catch (FileNotFoundException e) {
           System.err.println("Unable to find the file: fileName");
        }
        catch (IOException e) {
           System.err.println("Unable to read the file: fileName");
        }
   }

   	/* Return a MarketSnapshot of all stocks in marketIndexId (see MARKET_INDEX table)
    ** at specified dateTime. Quotes should come from STOCK_HISTORY table. Use CLOSE_PRICE. */
    public static MarketSnapshot getMarketSnapshot(int marketIndexId, Date dateTime) {
	
		ArrayList<ArrayList<Object>> results = execQtor(DB_QUERY + "date(\"" + df.format(dateTime) + "\");");
		//First ArrayList is the column names
		//Second ArrayList is the column types
		//ArrayList[i] for i>=2 are the rows
		HashMap<String, StockQuote> quotes = new HashMap<String, StockQuote>();
		//get colum for name and end price;
		int stockName = results.get(0).indexOf(SYM_COL);
		int stockPrice = results.get(0).indexOf(PRICE_COL);
		int stocks = results.size();
		String sName;
		double sPrice;

		for(int i=2; i<stocks; i++)
		{
			sName = (String) results.get(i).get(stockName);
			sPrice = ((BigDecimal) results.get(i).get(stockPrice)).doubleValue();
			quotes.put(sName, new StockQuote(sName, sPrice));
		}


		return new MarketSnapshot(quotes);
    }

	public static Date getStartDate()
	{
		ArrayList<ArrayList<Object>> sDate = execQtor("select min(TRADE_DATE) as td from STOCK_HISTORY;");
		return (Date) sDate.get(2).get(0);
	}

	public static Date getEndDate()
	{
		ArrayList<ArrayList<Object>> eDate = execQtor("select max(TRADE_DATE) as td from STOCK_HISTORY;");
		return (Date) eDate.get(2).get(0);
	}
    /* Takes a MARKET_INDEX_ID and returns an array of all the TRADING_SYMBOLs associated
    ** with that index. We need to pass this to each of the InvestmentStrategy classes to
    ** keep track of shares. */
    public static String[] getTradingSymbols(int marketIndexId)
	{
		String query = "select TRADING_SYMBOL from INSTRUMENT, (select INSTRUMENT_ID from INDEX_CMPSTN where MARKET_INDEX_ID=1) as indx where indx.INSTRUMENT_ID = INSTRUMENT.INSTRUMENT_ID;";
		ArrayList<ArrayList<Object>> symbols = execQtor(query);
		
		int numS = symbols.size();
		String[] s = new String[numS-2];
		for(int i=2; i<numS; i++) s[i-2] = (String) symbols.get(i).get(0);
		return s;
	}

	private static ArrayList<ArrayList<Object>> execQtor(String query)
	{
		
		Connection conn = null;
   		Statement stmt = null;
		ArrayList<ArrayList<Object>> results = new ArrayList<ArrayList<Object>>();

		try
		{
			Class.forName(JDBC_DRIVER);
		
      		conn = DriverManager.getConnection(DB_URL,USER,PASS);
			stmt = conn.createStatement();
			stmt.executeUpdate("use " + DB + ";");
			ResultSet rs = stmt.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			ArrayList<Object> names = new ArrayList<Object>(); //Was String
			ArrayList<Object> types = new ArrayList<Object>(); //Was Integer
			for(int i=1; i<=cols; i++)
			{
				names.add(rsmd.getColumnName(i));
				types.add(rsmd.getColumnType(i));
			}
			results.add(names);
			results.add(types);

			while(rs.next())
			{
				ArrayList<Object> row = new ArrayList<Object>();
				for(int i=1; i<=cols; i++)
					row.add(rs.getObject(i));
				results.add(row);
			}

			rs.close();
			
	//copied all of this**************
      stmt.close();
      conn.close();
   }catch(SQLException se){
      //Handle errors for JDBC
      se.printStackTrace();
   }catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();
   }finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            stmt.close();
      }catch(SQLException se2){
      }// nothing we can do
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try
   }//end try******************
   return results;
	}
}
