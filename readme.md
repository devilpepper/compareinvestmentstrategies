# Compare Investment Strategies - Project 2
In this project we need to compare 4 competing investments strategies outlined in the project assignment using the sample data provided in the Stockmarket database. To do this, we will break the assignment into subtasks. These subtasks are:

1. ![DONE][check] _Implement investment strategies._ An investment strategy makes a decision to buy or sell stocks in its portfolio based on a fixed rule. To preserve modularity, the implementation will be independent from querying the database. The investment strategy makes a decision based on a stream of input, the stock quotes, at particular time intervals. These stock quotes are represented in the MarketSnapshot object. For example, to calculate a moving average, we can store the 10 most recent MarketSnapshots and use these to calculate averages. Thus, at each time step we will pass a new MarketSnapshot to an investment strategy to represent the current state of the market. The strategies are outlined in the assignment: intersection of moving averages, buy the index, momentum, and increment (discussed in meeting with Dr. Barnett). We will not be implementing buy and hold selectively.
2. ![DONE][check] _Query the database._ This is implemented in the DatabaseQueries class. All access to the database should be handled by methods in the class.
3. ![DONE][check] _Run a market simulation._ This is implemented in the Simulator class. It runs through the data in the database to compare investment strategies over time and calculates the performance of each strategy.

For now, we will not use the database to buy and sell stocks. Stock quotes come from the STOCK_HISTORY table. We will use the CLOSE_PRICE when buying/selling stocks.

The InvestmentStrategy class gives a basic outline of how each InvestmentStrategy subclass should work. Please email with any questions.

[check]: http://www.free-icons-download.net/images/check-mark-icon-22236.png

## Compiling and running the program
### Dependencies
The program requires two text files to run. The first file, cred.txt, should contain the username and password for MySQL in the Linux Lab. The format of this file should be:

    USERNAME
    PASSWORD

The second required file, settings.txt, contains the start and end dates to look at in the stock history table, as well as the database and database URL to use.

    DATABASE=
    DATABASE_URL=
    START_DATE=01/01/2015
    END_DATE=12/01/2015
    INITIAL_CAPITAL=10000

    

# Compiling
The makefile provides all compile instructions. To compile the project, type

    make
To run a simulation with the above settings, type:

    make
    make simulate

To create an executable jar file, type:

    make 
    make jar
    
# Running the program

To run the program and save the output to a csv, type:

    java -jar InvestmentStrategies.jar > output.csv